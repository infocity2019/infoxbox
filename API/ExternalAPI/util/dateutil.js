const dateFormat = require('dateformat');
const _ = require('lodash');

module.exports = {
    formatDate: function (date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return year + month + day;
    },

    timeStamp: function () {
        var timeStamp = dateFormat(new Date(), "yyyymmddHHMMss");

        return timeStamp;
    },

    timeStamp2: function () {
        var timeStamp = dateFormat(new Date(), "yyyymmddHHMMl");

        return timeStamp;
    },

    getSeconds: function (start) {
        return ((new Date() - start) % 60000 / 1000).toFixed(2) + "s";
    },

    getCurrentInquiryDate: function () {
        return dateFormat(new Date(), 'yyyymmdd');
    },

    validDateAndCurrentDate: function (startDate, endDate) {
        if (!_.isEmpty(startDate) && _.isEmpty(endDate))
            return parseFloat(startDate.substring(0, 8)) <= parseFloat(this.getCurrentInquiryDate());
        else if (!(_.isEmpty(startDate) && _.isEmpty(endDate)))
            return parseFloat(startDate.substring(0, 8)) <= parseFloat(endDate.substring(0, 8));
        else
            return true;
    }

};