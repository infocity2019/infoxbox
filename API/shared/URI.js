
var URI = {

    cicInternalJson: 'https://1.234.53.23:9401/rest/extJson',

    internal_cic: 'https://localhost:3000/internal/cic',
    internal_cicB0003: 'https://localhost:3000/internal/cicB0003'
};

module.exports = URI;